# CatsSuggester
> [@CatsSuggester_bot](https://t.me/CatsSuggester_bot/)

![](images/demo1.jpg)      |  ![](images/demo2.jpg)
:-------------------------:|:-------------------------:

## Build

### Requirements

* Docker, Docker compose

### Instructions

1. Set up env variables, e.g. in `.env` file:
    ```
    bot_token="<TELEGRAM_BOT_TOKEN>"
    cats_api_key="<API_KEY>"
    ```
2. Build docker image:
    ```bash
    $ docker-compose build
    ```
3. Start the bot:
    ```bash
    $ docker-compose up
    ```

## TODO

### Necessary

- [X] Simple API wrapper for [thecatapi.com](https://thecatapi.com/).
- [X] Bot logic.
- [X] Saving everything in DB for future model.
- [X] Add authenticated users in DB.
- [ ] Add logic for waiting before `N` reviews from each user are collected for training model.
- [ ] Design ML model.

### Optional

- [X] Clean up available bot functions. Maybe rethink bot usage.
- [X] Some new features: get image now.
- [ ] Write good tests.
- [ ] Create CI/CD.
- [ ] Make one admin.
- [ ] Simple auth by admin.
