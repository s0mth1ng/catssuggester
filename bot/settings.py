from typing import List

from pydantic import BaseSettings


class Settings(BaseSettings):
    bot_token: str
    cats_api_key: str
    db_url: str
    db_name: str

    class Config:
        env_file = ".env"


settings = Settings()
