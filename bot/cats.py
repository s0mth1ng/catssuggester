import requests
from model.model import predict_proba

from bot.logger import logger
from bot.schemas import CatImage
from bot.settings import settings


def get_random_image() -> CatImage:
    headers = {"x-api-key": settings.cats_api_key}
    url = "https://api.thecatapi.com/v1/images/search"
    try:
        data = requests.get(url, headers=headers).json()
    except requests.exceptions.RequestException as e:
        logger.error(str(e))
        raise SystemExit(e)
    image = CatImage(url=data[0]["url"], id=data[0]["id"])
    logger.info(f"Loaded new image: {image.id}.")
    return image


def get_best_image(user_id: int, sample_size: int) -> CatImage:
    logger.info(f"Picking the best picture for user {user_id}.")
    images = [get_random_image() for _ in range(sample_size)]
    probas = [predict_proba(user_id, image) for image in images]
    best_ind = probas.index(max(probas))
    return images[best_ind]
