from dataclasses import dataclass
from typing import List

from pymongo import MongoClient

from bot.logger import logger
from bot.schemas import Review
from bot.settings import settings

client = MongoClient(settings.db_url)
db = client[settings.db_name]
logger.info("Connected to db.")
authenticated_users = [user["user_id"] for user in db["users"].find({})]
logger.info(f"Authenticated users: {authenticated_users}")


def put_review(review: Review) -> None:
    logger.info("Review added to db.")
    db["reviews"].insert_one(review.dict())


def get_reviews(user_id: int) -> List[Review]:
    jsons = db["reviews"].find({"user_id": user_id})
    return [Review(**json) for json in jsons]


def subscribe_user(user_id: int, minutes: int) -> None:
    db["users"].update_one({"user_id": user_id}, {"$set": {"minutes": minutes}})


def unsubscribe_user(user_id: int) -> None:
    db["users"].update_one({"user_id": user_id}, {"$set": {"minutes": 0}})


@dataclass
class UserInfo:
    total_reviews: int
    liked: int
    minutes: int


def get_user_info(user_id: int) -> UserInfo:
    return UserInfo(
        total_reviews=db["reviews"].count_documents({"user_id": user_id}),
        liked=db["reviews"].count_documents({"user_id": user_id, "liked": True}),
        minutes=db["users"].find_one({"user_id": user_id})["minutes"],
    )
