import json
from dataclasses import asdict, dataclass
from typing import Dict, List

from telegram import ForceReply, InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import (
    CallbackContext,
    CallbackQueryHandler,
    CommandHandler,
    ConversationHandler,
    Filters,
    MessageHandler,
    Updater,
)

from bot.cats import CatImage, get_best_image
from bot.db import (
    UserInfo,
    authenticated_users,
    get_user_info,
    put_review,
    subscribe_user,
    unsubscribe_user,
)
from bot.logger import logger
from bot.schemas import Review
from bot.settings import settings

images: List[CatImage] = []


@dataclass
class UserAnswer:
    liked: bool
    image_ind: int

    def to_json(self) -> str:
        return json.dumps(asdict(self))

    def to_dict(self) -> Dict:
        return asdict(self)


def check_user(update: Update) -> bool:
    user = update.message.from_user
    if user["id"] not in authenticated_users:
        logger.warning(f'Someone (user_id = {user["id"]}) found the bot.')
        update.message.reply_text("Go away 🐈🐈🐈.")
        return False
    return True


def start(update: Update, _: CallbackContext) -> None:
    """Sends explanation on how to use the bot."""
    if not check_user(update):
        return
    update.message.reply_text("Hi! Use /sub to subscribe to CatsSuggester 😼")


def send_image(context: CallbackContext) -> None:
    """Sends a message with three inline buttons attached."""
    image = get_best_image(context.job.context["user_id"], sample_size=1)
    global images
    images.append(image)
    keyboard = [
        [
            InlineKeyboardButton(
                "😍", callback_data=UserAnswer(True, len(images) - 1).to_json()
            ),
            InlineKeyboardButton(
                "😒", callback_data=UserAnswer(False, len(images) - 1).to_json()
            ),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    context.bot.send_photo(
        context.job.context["chat_id"], image.url, reply_markup=reply_markup
    )


def button_callback(update: Update, _: CallbackContext) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    query.answer()
    answer = UserAnswer(**json.loads(query.data))
    user = query.from_user
    review = Review(
        user_id=user["id"], image_url=images[answer.image_ind].url, liked=False
    )
    if not answer.liked:
        logger.info(
            f"User {user['id']} did not like the image {images[answer.image_ind].id}."
        )
        query.delete_message()
    else:
        logger.info(f"User {user['id']} liked the image {images[answer.image_ind].id}.")
        query.edit_message_reply_markup()
        review.liked = True
    put_review(review)


def remove_job_if_exists(name: str, context: CallbackContext) -> bool:
    """Remove job with given name. Returns whether job was removed."""
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


def subscribe(update: Update, context: CallbackContext) -> int:
    if not check_user(update):
        return
    context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Duration in minutes (at least 5):",
    )
    return 0


def set_timer(update: Update, context: CallbackContext) -> None:
    """Add a job to the queue."""
    if not check_user(update):
        return
    chat_id = update.message.chat_id
    minutes = int(update.message.text)
    remove_job_if_exists(str(chat_id), context)
    user = update.message.from_user
    context.job_queue.run_repeating(
        send_image,
        60 * minutes,
        context={"chat_id": chat_id, "user_id": user["id"]},
        name=str(chat_id),
    )
    text = f"You will receive cats images every {minutes} minutes 😽"
    subscribe_user(user["id"], minutes)
    logger.info(f"User {user['id']} subscribed. Sending every {minutes} minutes.")
    update.message.reply_text(text)
    return ConversationHandler.END


def unsubscribe(update: Update, context: CallbackContext) -> None:
    """Remove the job if the user changed their mind."""
    if not check_user(update):
        return
    chat_id = update.message.chat_id
    job_removed = remove_job_if_exists(str(chat_id), context)
    text = (
        "You unsubscribed from cats suggester 😥"
        if job_removed
        else "You don't have a subscription yet 🤯"
    )
    user = update.message.from_user
    unsubscribe_user(user["id"])
    logger.info(f"User {user['id']} unsubscribed.")
    update.message.reply_text(text)


def info_description(info: UserInfo) -> str:
    description = "📨 *Subscription*\n"
    if info.minutes:
        description += f"🕑 You are receiving pics every *{info.minutes}* minutes.\n"
    else:
        description += "You are _not_ subscribed 😿\n"
    description += "\n🥇 *Statistics*\n"
    if not info.total_reviews:
        description += "You haven't reviewed any pics yet 😿"
    elif info.total_reviews == 1:
        description += "You reviewed only one pic. What did you expect to see here? 🙃"
    description += (
        f"You have reviewed *{info.total_reviews}* pics in total 🙀\n"
        f"Among them you _liked_ exactly *{info.liked}* pics 😼\n"
        f"It's about *{info.liked * 100 / info.total_reviews:.1f}%* 😽"
    )
    return description


def get_info(update: Update, context: CallbackContext) -> None:
    """Gets information about user progress."""
    if not check_user(update):
        return
    user_id = update.message.from_user["id"]
    info = get_user_info(user_id)
    text = info_description(info)
    logger.info(f"User {user_id} requested info. He has {info.total_reviews} reviews.")
    update.message.reply_text(text, parse_mode="markdown")


def cancel(update: Update, _: CallbackContext) -> int:
    """Sends explanation on how to use the bot."""
    if not check_user(update):
        return
    update.message.reply_text(
        "*Integer* number of minutes and *at least 5*. Pretty simple... 🤓",
        parse_mode="markdown",
    )
    return ConversationHandler.END


def send_image_now(update: Update, context: CallbackContext) -> None:
    if not check_user(update):
        return
    chat_id = update.message.chat_id
    user = update.message.from_user
    context.job_queue.run_once(
        send_image,
        when=0,
        context={"chat_id": chat_id, "user_id": user["id"]},
        name=str(chat_id),
    )


def start_bot() -> None:
    """Run the bot."""
    updater = Updater(settings.bot_token)
    updater.dispatcher.add_handler(CommandHandler("start", start))
    updater.dispatcher.add_handler(CallbackQueryHandler(button_callback))
    updater.dispatcher.add_handler(
        ConversationHandler(
            entry_points=[CommandHandler("sub", subscribe)],
            states={
                0: [MessageHandler(Filters.regex(r"^(5|6|7|8|9|\d\d+)$"), set_timer)],
            },
            fallbacks=[MessageHandler(Filters.all, cancel)],
        )
    )
    updater.dispatcher.add_handler(CommandHandler("unsub", unsubscribe))
    updater.dispatcher.add_handler(CommandHandler("info", get_info))
    updater.dispatcher.add_handler(CommandHandler("get", send_image_now))
    updater.start_polling()
    updater.idle()
