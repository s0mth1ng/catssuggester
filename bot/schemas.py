from pydantic import BaseModel


class Review(BaseModel):
    user_id: int
    image_url: str
    liked: bool


class CatImage(BaseModel):
    url: str
    id: str
