FROM python:3.8-slim
RUN pip install poetry
WORKDIR /code
COPY poetry.lock pyproject.toml /code/
RUN POETRY_VIRTUALENVS_CREATE=false poetry install
COPY . /code
CMD ["python", "main.py"]
