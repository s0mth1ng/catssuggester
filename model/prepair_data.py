import argparse
import os
import pathlib
from os.path import join
from typing import List

import requests
from bot.db import authenticated_users, get_reviews
from bot.schemas import Review
from tqdm import tqdm


def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Download images from the web for users."
    )
    parser.add_argument(
        "users",
        metavar="user_id",
        type=int,
        nargs="*",
        help="specific user to download data for",
    )
    parser.add_argument(
        "--to",
        type=pathlib.Path,
        help="folder to download into (by default ./data/)",
        default="./data/",
    )
    return parser


def clean_up(reviews: List[Review]):
    urls = set()
    cleaned: List[Review] = []
    for r in reviews:
        if not r.image_url in urls:
            urls.add(r.image_url)
            cleaned.append(r)
    return cleaned


def filename_by_url(url: str) -> str:
    return f"{url.split('/')[-1]}"


def main() -> None:
    parser = init_argparse()
    args = parser.parse_args()
    users = args.users if args.users else authenticated_users[:]
    path = args.to
    if not os.path.isdir(path):
        print(f"{path} is not a directory")
        exit(-1)
    for user in users:
        print(f"Processing user: {user}")
        user_folder = join(path, str(user))
        images_folder = join(user_folder, "images")
        reviews_csv = join(user_folder, "reviews.csv")
        if not os.path.exists(user_folder):
            os.mkdir(user_folder)
        if not os.path.exists(images_folder):
            os.mkdir(images_folder)
        downloaded = {}
        if os.path.exists(reviews_csv):
            with open(reviews_csv, "r") as f:
                downloaded = set(r.split(",")[0] for r in f.read().split("\n")[1:])
        else:
            with open(reviews_csv, "w") as f:
                f.write("image,liked")
        reviews = get_reviews(user)
        reviews = clean_up(reviews)
        csv_data = []
        for review in tqdm(reviews):
            image = filename_by_url(review.image_url)
            if image in downloaded:
                continue
            response = requests.get(review.image_url, stream=True)
            if response.ok:
                with open(
                    join(images_folder, f"{image}"),
                    "wb",
                ) as f:
                    for chunk in response:
                        f.write(chunk)
                csv_data.append((image, review.liked))
        with open(join(user_folder, "reviews.csv"), "a") as f:
            f.write("\n" + "\n".join([f"{im},{int(li)}" for im, li in csv_data]))


if __name__ == "__main__":
    main()
